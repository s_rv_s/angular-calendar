var express = require("express");
var path = require("path");
var mongo = require("mongoose");
var bodyParser = require("body-parser");
var cookieParcer = require("cookie-parser");
var fs = require("fs");
var jwt = require("jsonwebtoken");
var pretty = require("express-prettify");

var app = express();
app.use(bodyParser());
app.use(cookieParcer());
app.use(bodyParser.json({limit: "5mb"}));
app.use(bodyParser.urlencoded({extended: true}));
app.use(pretty({ query: "pretty" }));

var RSA_PRIVATE_KEY = fs.readFileSync("./src/jwtRS256.key");

var database = mongo.connect("mongodb://localhost:27017/angularCalendar", function (err, response) {
  if (err) {
    // tslint:disable-next-line:no-console
    console.log( err);
  } else {
    // tslint:disable-next-line:no-console
    console.log("Connected to database");
  }
}, {useNewUrlParser: true});

app.use(function(req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "http://localhost:4200");
  res.setHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");
  res.setHeader("Access-Control-Allow-Headers", "X-Requested-With,content-type,Authorization");
  res.setHeader("Access-Control-Allow-Credentials", true);
  next();
});

var Schema = mongo.Schema;

var CalendarEventsSchema = new Schema({
  duration: { type: Number },
  offset: { type: Number },
  start: { type: Number },
  title: { type: String }}).set("toJSON", {
  virtuals: true,
  versionKey: false,
  transform: function(doc, ret) {   delete ret._id  }
});

var calendarEventsModel = mongo.model("calendarEvents", CalendarEventsSchema, "calendarEvents");
  // check and feel database :)
  calendarEventsModel.find({}, function(err, data) {
     if ( !data.length) {
       // tslint:disable-next-line:no-console
       console.log("filling database from mock");
       calendarEventsModel.create(eventsMock, function(err, data) {
         if (err) {
           // tslint:disable-next-line:no-console
           console.error("error to fill database from mock");
         } else {
           // tslint:disable-next-line:no-console
           console.log("success filled database from mock");
         }
       });
     }
  });

// var router = express.Router([]);

app.post("/api/login", function(req, res) {
  if (!checkAuth(req, res)) {
    return;
  }
  if (req.body && req.body.username) {
    var user = { name: userMock.username + " :()", token: userMock.token};
    res.send(user);
  } else {
    res.status(500).error("error");
  }
});

app.route("/api/calendar-events")
  .get(function(req, res) {
    if (!checkAuth(req, res)) {
      return;
    }
    calendarEventsModel.find({}, function(err, data) {
      if (err) {
        res.status(500).error("error");
      } else {
        res.json(data);
      }
    });
  })
  .post(function(req, res) {
    if (!checkAuth(req, res)) {
      return;
    }
    calendarEventsModel.create(req.body, function(err, data) {
      if (err) {
        res.status(500).error("error");
      } else {
        res.json(data);
      }});
  });

app.route("/api/calendar-events/export")
  .get(function(req, res) {
    if (!checkAuth(req, res)) {
      return;
    }
    calendarEventsModel.find({}, eventExportFields, function(err, data) {
      if (err) {
        res.status(500).error("error");
      } else {
        res.json(data);
      }
    });
  });

app.route("/api/calendar-events/:eventId")
  .delete(function(req, res) {
    if (!checkAuth(req, res)) {
      return;
    }
    calendarEventsModel.deleteOne({_id: req.params.eventId}, function(err, data) {
      if (err) {
        res.status(204).error("error");
      } else {
        res.sendStatus(204);
      }
    });
  });

var checkAuth = function(req, res) {
  if (req.header("Authorization") === userMock.token ||
    req.body.username === userMock.username && req.body.password === userMock.password) {
    // res.cookie("token", userMock.token, {httpOnly: true});
    return true;
  } else {
    // send status 401 Unauthorized
    res.sendStatus(401);
    return false;
  }
};

app.listen(8080, function() {
  // tslint:disable-next-line:no-console
  console.log("Example app listening on port 8080!");
});

var userMock = { username: "user", password: "pass", token: jwt.sign({}, RSA_PRIVATE_KEY, {
    algorithm: "RS256",
    expiresIn: 0,
    subject: "user" })};

var eventExportFields = {
  duration: 1,
  start: 1,
  title: 1
};
var eventsMock = [
  { id: 1,
    title: "Exercise",
    description: "",
    start: 0,
    duration: 15,
    offset: 480 },
  {
    id: 2,
    title: "Travel to work",
    description: "",
    start: 25,
    duration: 30,
    offset: 480 },
  {
    id: 3,
    title: "Plan day",
    description: "",
    start: 30,
    duration: 30,
    offset: 480 },
  {
    id: 4,
    title: "Review yesterday`s commits",
    description: "",
    start: 60,
    duration: 15,
    offset: 480 },
  {
    id: 5,
    title: "Code review",
    description: "",
    start: 100,
    duration: 15,
    offset: 480 },
  {
    id: 6,
    title: "Have lunch with John",
    description: "",
    start: 180,
    duration: 90,
    offset: 480 },
  {
    id: 7,
    title: "Skype call",
    description: "",
    start: 360,
    duration: 30,
    offset: 480 },
  {
    id: 8,
    title: "Follow up with designer",
    description: "",
    start: 370,
    duration: 45,
    offset: 480 },
  {
    id: 9,
    title: "Push up branch",
    description: "",
    start: 405,
    duration: 30,
    offset: 480 }
];
