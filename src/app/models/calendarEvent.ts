export interface CalendarEvent {
  id: string;
  title: string;
  description?: string;
  start: number;
  duration: number;
  offset: number;
}

export class CalendarEventInstance implements CalendarEvent {
  id: string;
  title: string;
  start: number;
  duration: number;
  description: string;
  offset: number;
}
