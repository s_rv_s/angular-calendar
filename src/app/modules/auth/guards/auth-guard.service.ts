import {Injectable} from '@angular/core';
import {CanActivate} from '@angular/router';
import {Observable} from 'rxjs';
import * as fromAuth from '../store/reducers';
import {select, Store} from '@ngrx/store';
import {map, take} from 'rxjs/operators';
import {AuthActions} from '../store/actions';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements  CanActivate {
  constructor(private store: Store<fromAuth.State>) {
  }

  canActivate(): Observable<boolean> {
    return this.store.pipe(
      select(fromAuth.getLoggedIn),
      map(isLoggedId => {
        if (!isLoggedId) {
          this.store.dispatch(AuthActions.logout());
        }
        return isLoggedId;
      }),
      take(1)
    );
  }
}
