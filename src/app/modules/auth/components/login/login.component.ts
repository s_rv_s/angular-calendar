import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthActions} from '../../store/actions';
import {Store} from '@ngrx/store';
import * as authReducers from '../../store/reducers';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: FormGroup = new FormGroup({
    username: new FormControl('user'),
    password: new FormControl('')
  });

  constructor(private store: Store<authReducers.State>,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
  }

  submit() {
    if (this.form.valid) {
      this.store.dispatch(AuthActions.login(this.form.value));
    }
  }
}
