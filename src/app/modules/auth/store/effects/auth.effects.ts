import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {AuthActions, AuthApiActions} from '../actions';
import {catchError, exhaustMap, map, tap} from 'rxjs/operators';
import {Credentials} from '../../models/credentials';
import {User} from '../../models/user';
import {AuthService} from '../../services/auth.service';
import {of} from 'rxjs';

@Injectable()
export class AuthEffects {
  constructor(
    private actions$: Actions,
    private authService: AuthService,
    private router: Router
  ) {}

  @Effect()
  login$ = this.actions$.pipe(
    ofType(AuthActions.login.type),
    // map(action => action.credentials),
    exhaustMap((auth: Credentials) => {
        return this.authService.login(auth).pipe(
          map((user: User) =>  AuthApiActions.loginSuccess({user})),
          catchError(error => of(AuthApiActions.loginFailure({error})))
        );
      }
    )
  );

  @Effect({ dispatch: false })
  loginSuccess$ = this.actions$.pipe(
    ofType(AuthApiActions.loginSuccess.type),
    tap( () => this.router.navigate(['/calendar']))
  );

  @Effect({ dispatch: false })
  logout$ = this.actions$.pipe(
    ofType(AuthActions.logout.type),
    tap( () => this.router.navigate(['/login']))
  );
}
