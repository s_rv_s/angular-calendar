import {createAction, props, union} from '@ngrx/store';
import {Credentials} from '../../models/credentials';

export const logout = createAction('[Auth] Logout');

export const login = createAction(
  '[Auth] Login',
  props< {credentials: Credentials} >()
);

const all = union({login, logout});
export type AuthActionsUnion = typeof all;
