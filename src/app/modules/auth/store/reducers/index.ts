import * as fromRoot from '../../../../store/reduces/app-reducers';
import {ActionReducerMap, createFeatureSelector, createSelector} from '@ngrx/store';

import * as fromAuth from './auth.reducer';
import {User} from '../../models/user';

export interface AuthState {
  status: fromAuth.State;
}

export interface State extends fromRoot.State {
  auth: AuthState;
}

export const reducers: ActionReducerMap<AuthState, any> = {
  status: fromAuth.reducer
};

export const selectAuthState = createFeatureSelector<State, AuthState>('auth');

export const selectAuthStatusState = createSelector(
  selectAuthState,
  (state: AuthState) => state.status
);
export const getUser = createSelector(selectAuthStatusState, fromAuth.getUser);
export const getUserName = createSelector(selectAuthStatusState, fromAuth.getUserName);
export const getLoggedIn = createSelector(getUser, user => !!user);
export const getToken = createSelector(getUser, user => user ? user.token : '');
