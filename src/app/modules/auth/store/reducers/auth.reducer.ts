import {User} from '../../models/user';
import {AuthActions, AuthApiActions} from '../actions';

export interface State {
  user: User | null;
}

export const initialState: State = {
  user: null
};

export function reducer(state: State = initialState,
                        action: AuthActions.AuthActionsUnion | AuthApiActions.AuthApiActionsUnion) {
  switch (action.type) {
    case AuthApiActions.loginSuccess.type: {
      return {
        ...state,
        user: action.user
      };
    }
    case AuthActions.logout.type: {
      return initialState;
    }
    default: {
      return state;
    }
  }
}

export const getUser = (state) => state.user;
export const getUserName = (state) => state.user.name;
