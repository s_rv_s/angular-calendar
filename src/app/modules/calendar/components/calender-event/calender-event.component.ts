import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CalendarEvent} from '../../../../models/calendarEvent';
import {Store} from '@ngrx/store';
import * as calendarReducers from '../../store/reducers';
import {CalendarAction} from '../../store/actions';

@Component({
  selector: 'app-calender-event',
  templateUrl: './calender-event.component.html',
  styleUrls: ['./calender-event.component.css']
})

export class CalenderEventComponent implements OnInit {
  @Input() calendarEvent: CalendarEvent;
  // @Output() select = new EventEmitter();
  constructor(private store: Store<calendarReducers.State>) { }

  ngOnInit() {
  }

  getTime() {
    const hours = Math.floor((this.calendarEvent.offset + this.calendarEvent.start) / 60);
    const minutes = (this.calendarEvent.offset + this.calendarEvent.start) % 60;
    return ((hours < 10 ? '0' : '') + hours + ':' + (minutes < 10 ? '0' : '') + minutes);
  }

  removeEvent(eventId: string) {
    this.store.dispatch(CalendarAction.removeEvent({ eventId }));
  }
}
