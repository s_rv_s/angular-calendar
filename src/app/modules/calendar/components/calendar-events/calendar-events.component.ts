import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {CalendarEvent, CalendarEventInstance} from '../../../../models/calendarEvent';
import {Observable} from 'rxjs';
import {Store} from '@ngrx/store';
import * as calendarReducers from '../../store/reducers';
import * as authReducers from '../../../auth/store/reducers';
import {ActivatedRoute, Router} from '@angular/router';
import {CalendarAction} from '../../store/actions';
import {map, take, tap} from 'rxjs/operators';

@Component({
  selector: 'app-calendar-events',
  templateUrl: './calendar-events.component.html',
  styleUrls: ['./calendar-events.component.css']
})
export class CalendarEventsComponent implements OnInit, OnChanges {
  @Input() userStart: number;
  @Input() userEnd: number;
  @Output() select = new EventEmitter();
  calendarEvents$: Observable<CalendarEvent[]>;
  calendarEventSelected$: Observable<CalendarEvent>;
  calendarEventsWidth: { [id: number]: { width: number, orderPos: number } } = {};
  timeOffsets: {offset: number, time: string}[] = [];
  scale: number;
  user$: Observable<string>;

  constructor(private store: Store<calendarReducers.State>,
              private authStore: Store<authReducers.State>,
              private route: ActivatedRoute,
              private router: Router) {
    this.calendarEvents$ = store.select(calendarReducers.getEvents);
    this.calendarEventSelected$ = store.select(calendarReducers.getSelectedEvent);
    this.user$ = authStore.select(authReducers.getUserName);
  }

  ngOnChanges(changes) {
  }

  ngOnInit() {
    this.scale = 1.5;
    this.calendarEvents$.subscribe((ce) => {
      this.mapEventsWidth(ce);
      this.mapTimeOffsets();
    });
    this.store.dispatch(CalendarAction.load);
  }

  private mapEventsWidth(calendarEvents) {
    const orderedEvents = [].concat(calendarEvents).sort((ce1, ce2) => ((ce1.start + ce1.offset) - (ce2.start + ce2.offset)));
    orderedEvents.forEach((ce1, idx, list) => {
      const eventsNextInCollision = list.slice(idx + 1)
        .filter((ce2) => ((ce1.start + ce1.offset < ce2.start + ce2.offset) &&
                          ((ce1.start + ce1.offset + ce1.duration) > ce2.start + ce2.offset)));
      const eventsPrevInCollision = list.slice(0, idx)
        .filter((ce2) => ((ce2.start + ce2.offset < ce1.start + ce1.offset) &&
                          ((ce2.start + ce2.offset + ce2.duration) > ce1.start + ce1.offset)));
      const eventsInCollision = [].concat(eventsPrevInCollision, [ce1], eventsNextInCollision);
      this.calendarEventsWidth[ce1.id] = {
        width: eventsInCollision.length === 0 ? 1 : parseFloat((1 / eventsInCollision.length).toFixed(2)),
        orderPos: (eventsInCollision.findIndex((ce2) => ce2.id === ce1.id) + 1)
      };
    });
  }

  private mapTimeOffsets() {
    const userStart = this.userStart || 480;
    const userEnd = this.userEnd || userStart + 570;
    let offset = 0;
    this.timeOffsets = [];
    while (offset < userEnd - userStart) {
      const hours = Math.floor((userStart + offset) / 60);
      const minutes = (userStart + offset) % 60;
      this.timeOffsets.push({
        offset: offset,
        time: ((hours < 10 ? '0' : '') + hours + ':' + (minutes < 10 ? '0' : '') + minutes)
      });
      offset += 30;
    }
  }
  createNewCalendarEvent() {
    const event = new CalendarEventInstance();
    this.store.dispatch(CalendarAction.select({event}));
  }

  exportCalendarEvents() {
    this.store.select(calendarReducers.getEvents)
      .pipe(
        map( events => this.store.dispatch(CalendarAction.exportEvents({events}))),
        take(1))
      .subscribe(events => {});
  }
}
