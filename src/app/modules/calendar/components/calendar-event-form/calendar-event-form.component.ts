import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Store} from '@ngrx/store';
import * as calendarReducers from '../../store/reducers';
import {CalendarEvent} from '../../../../models/calendarEvent';
import {CalendarAction} from '../../store/actions';

@Component({
  selector: 'app-calendar-event-form',
  templateUrl: './calendar-event-form.component.html',
  styleUrls: ['./calendar-event-form.component.css']
})
export class CalendarEventFormComponent implements OnInit {
  @Input() calendarEvent: CalendarEvent;

  constructor(private store: Store<calendarReducers.State>) { }

  ngOnInit() {
    /*this.form = new FormGroup({
      title: new FormControl('', Validators.required),
      start: new FormControl('', [Validators.required, Validators.min(0), Validators.max(540)]),
      duration: new FormControl('', [Validators.required, Validators.min(5), Validators.max(565)]),
      offset: new FormControl('480'),
    });*/
  }

  submit(form, event: CalendarEvent) {
    if (form.valid) {
      event.offset = event.offset || 480;
      this.store.dispatch(CalendarAction.addEvent({event}));
    }
  }

  cancel() {
    this.store.dispatch(CalendarAction.select({event: null}));
  }
}
