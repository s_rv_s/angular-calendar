import {NgModule} from '@angular/core';
import {CalenderEventComponent} from './components/calender-event/calender-event.component';
import {CalendarEventsComponent} from './components/calendar-events/calendar-events.component';
import {CalendarEventFormComponent} from './components/calendar-event-form/calendar-event-form.component';
import {CalendarRoutingModule} from './calendar-routing.module';
import {StoreModule} from '@ngrx/store';
import {reducers} from './store/reducers';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {EffectsModule} from '@ngrx/effects';
import {CalendarEffects} from './store/effects/calendar.effects';
import {FormsModule} from '@angular/forms';
import {FileSaverModule} from 'ngx-filesaver';

const components = [
  CalenderEventComponent,
  CalendarEventsComponent,
  CalendarEventFormComponent
];

@NgModule({
  declarations: [
    ...components
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    FileSaverModule,
    StoreModule.forFeature('calendar', reducers),

    EffectsModule.forFeature([CalendarEffects]),
    CalendarRoutingModule
  ],
  entryComponents: [
    ...components
  ]
})
export class CalendarModule { }
