import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CalendarEventsComponent} from './components/calendar-events/calendar-events.component';


const calendarRoutes: Routes = [
  {
    path: '', component: CalendarEventsComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(
      calendarRoutes
    )
  ],
  exports: [
    RouterModule
  ]
})
export class CalendarRoutingModule { }
