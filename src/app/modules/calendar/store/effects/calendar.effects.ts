import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {CalendarEventsService} from '../../services/calendar-events.service';
import {EMPTY} from 'rxjs';
import {catchError, map, mergeMap, tap} from 'rxjs/operators';
import {CalendarAction} from '../actions';

@Injectable()
export class CalendarEffects {
  constructor(
    private action$: Actions,
    private calendarEventService: CalendarEventsService
  ) {

  }

  @Effect()
  loadEvents$ = this.action$.pipe(
      ofType(CalendarAction.load.type),
      mergeMap(() => this.calendarEventService.getAll()
        .pipe(
          map((events) => CalendarAction.loadSuccess( {events} )),
          catchError(() => EMPTY)
        ))
    );

  @Effect()
  removeEvent$ = this.action$.pipe(
      ofType(CalendarAction.removeEvent.type),
      map( (action) => {
        // @ts-ignore
        return action.eventId;
      } ),
      mergeMap((eventId) => this.calendarEventService.removeEvent(eventId)
        .pipe(
          map(() => CalendarAction.removeEventSuccess( {eventId} )),
          catchError(() => EMPTY)
        ))
    );

  @Effect()
  addEvent$ = this.action$.pipe(
      ofType(CalendarAction.addEvent.type),
      map( (action) => {
        // @ts-ignore
        return action.event;
      } ),
      mergeMap((eventCreate) => this.calendarEventService.addEvent(eventCreate)
        .pipe(
          map((event) => CalendarAction.addEventSuccess( { event } )),
          catchError(() => EMPTY)
        ))
    );

  @Effect({dispatch: false})
  exportEvents$ = this.action$.pipe(
      ofType(CalendarAction.exportEvents.type),
      map( (action) => {
        // @ts-ignore
        return action.events;
      }),
      map(events => this.calendarEventService.exportEvents(events))
    );
}
