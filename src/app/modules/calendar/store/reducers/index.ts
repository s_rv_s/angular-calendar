import {ActionReducerMap, createFeatureSelector, createSelector} from '@ngrx/store';
import * as fromCalendar from './calendar.reducer';
import * as fromRoot from '../../../../store/reduces/app-reducers';

export interface CalendarStatusState {
  status: fromCalendar.State;
}

export interface State extends fromRoot.State {
  calendar: CalendarStatusState;
}

export const reducers: ActionReducerMap<CalendarStatusState> = {
  status: fromCalendar.reducer
};

export const selectCalendarState = createFeatureSelector<State, CalendarStatusState>('calendar');

export const selectCalendarStatusState = createSelector(
  selectCalendarState,
  (state: CalendarStatusState) => state.status
);

export const getEvents = createSelector(
  selectCalendarStatusState, fromCalendar.getEvents
);

export const getSelectedEvent = createSelector(
  selectCalendarStatusState, fromCalendar.getSelectedEvent
);
