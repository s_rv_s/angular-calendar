import {CalendarEvent} from '../../../../models/calendarEvent';
import {CalendarAction} from '../actions';

export interface State {
  events: CalendarEvent[];
  selected: CalendarEvent;
}

export const initialState: State = {
  events: [],
  selected: null
};

export function reducer(state: State = initialState, action: CalendarAction.CalendarActionUnion): State {
  switch (action.type) {
    case CalendarAction.removeEventSuccess.type: {
      const newEvents = [].concat(state.events);
      const eventId = newEvents.findIndex(event => event.id === action.eventId);
      if (eventId !== -1) {
        newEvents.splice(eventId, 1);
      }
      return {
        ...state,
        events: newEvents
      };
    }

    case CalendarAction.addEventSuccess.type: {
      const newEvents = [].concat(state.events).concat([action.event]);
      return {
        ...state,
        events: newEvents,
        selected: null
      };
    }

    case CalendarAction.select.type: {
      return {
        ...state,
        selected: action.event
      };
    }

    case CalendarAction.loadSuccess.type: {
      return {
        ...state,
        events: action.events
      };
    }

    default:
      return state;
  }
}

export const getEvents = (state: State) => state.events;
export const getSelectedEvent = (state: State) => state.selected;

