import {createAction, props, union} from '@ngrx/store';
import {CalendarEvent} from '../../../../models/calendarEvent';

export const load = createAction(
  '[Calendar events] load');

export const loadSuccess = createAction(
  '[Calendar events] Loaded success',
  props<{ events: CalendarEvent[]}>()
);

export const removeEvent = createAction(
  '[Calendar event] remove one',
  props< { eventId: string }>()
);

export const removeEventSuccess = createAction(
  '[Calendar event] remove one success',
  props< { eventId: string }>()
);

export const addEvent = createAction(
  '[Calendar events] Add event',
  props<{ event: CalendarEvent }>()
);

export const addEventSuccess = createAction(
  '[Calendar events] Added event success',
  props<{ event: CalendarEvent }>()
);

export const select = createAction(
  '[Calendar events] Select event',
  props<{ event: CalendarEvent | null }>()
);

export const exportEvents = createAction(
  '[Calendar events] Export events',
  props<{ events: CalendarEvent[] }>()
);

const all = union({load, loadSuccess, removeEvent, removeEventSuccess, addEvent, addEventSuccess, select, exportEvents});

export type CalendarActionUnion = typeof all;
