import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CalendarEvent} from '../../../models/calendarEvent';
import {FileSaverService} from 'ngx-filesaver';

@Injectable({
  providedIn: 'root'
})
export class CalendarEventsService {

  constructor(private http: HttpClient,
              private fileSaverService: FileSaverService) { }

  getAll() {
    return this.http.get<CalendarEvent[]>('/api/calendar-events');
  }

  removeEvent(eventId: string) {
    return this.http.delete<string>('/api/calendar-events/' + eventId);
  }

  addEvent(event: CalendarEvent) {
    return this.http.post<CalendarEvent>('/api/calendar-events', event);
  }

  exportEvents(events: CalendarEvent[]) {
    // const blob = new Blob([JSON.stringify(events)], { type: 'application/json' });
    // const url = window.URL.createObjectURL(blob);
    return this.http.get('/api/calendar-events/export', {responseType: 'blob'}).subscribe(res => {
      this.fileSaverService.save(res, 'events.json');
    });
  }
}
