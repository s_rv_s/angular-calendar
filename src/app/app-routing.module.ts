import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from './modules/auth/guards/auth-guard.service';


const appRoutes: Routes = [
  { path: '', redirectTo: '/calendar', pathMatch: 'full'},
  {
    path: 'calendar',
    loadChildren: './modules/calendar/calendar.module#CalendarModule',
    canActivate: [AuthGuard]
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot( appRoutes,  { useHash: true } )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
