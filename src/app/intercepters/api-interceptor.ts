import {Injectable} from '@angular/core';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class APIInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const url = (req.url.match(/^.+:\/\/.+/)
             ? req.url
             : `http://localhost:8080/${req.url.replace(/^\/?(.*)/, '$1')}`);
    const apiReq = req.clone({ url });
    return next.handle(apiReq);
  }
}
