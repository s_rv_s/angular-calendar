import {HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {select, Store} from '@ngrx/store';
import * as fromAuth from '../modules/auth/store/reducers';
import {Observable, of} from 'rxjs';
import {first, map, mergeMap} from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private store: Store<fromAuth.State>) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    return this.addToken(req).pipe(
      first(),
      mergeMap((requestWithToken: HttpRequest<any>) => next.handle(requestWithToken))
    );
  }

  private addToken(request: HttpRequest<any>): Observable<HttpRequest<any>> {
    return this.store.pipe(
      select(fromAuth.getToken),
      first(),
      mergeMap((token: string) => {
        if (token) {
          request = request.clone({
            headers: request.headers.set('Authorization', token),
            withCredentials: true
          });
        }
        return of(request);
      })
    );
  }
}
