import {Action} from '@ngrx/store';

export const enum RooActionTypes {
  APP_NAME = '[root store] app name'
}

class AppName implements Action {
  readonly type: RooActionTypes.APP_NAME;
  constructor() {}
}

export type Action = AppName;
