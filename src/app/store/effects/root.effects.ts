import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {RooActionTypes} from '../actions/app-actions';
import {map} from 'rxjs/operators';

@Injectable()
export class RootEffects {
  constructor(
    private actions$: Actions) {
  }
  @Effect()
  getAppName$ = this.actions$.pipe(
    ofType(RooActionTypes.APP_NAME),
    map(appName => console.log('app Name: ', appName))
  );
}
