import * as fromAppState from './app-reducers';
import {ActionReducerMap, createFeatureSelector, createSelector} from '@ngrx/store';
import {routerReducer, RouterReducerState} from '@ngrx/router-store';

export interface State {
  root: fromAppState.State;
  router: RouterReducerState;
}

export const reducers: ActionReducerMap<State> = {
  router: routerReducer,
  root: fromAppState.reducer
};

export const getRootState = createFeatureSelector<State>('root');

export const getAppName = createSelector(
  getRootState, (state: State) => state.root
);
