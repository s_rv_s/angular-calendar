import * as appActions from '../actions/app-actions';
import {RooActionTypes} from '../actions/app-actions';

export interface State {
  appName: string;
}

export const initialState: State = {
  appName: 'Angular calendar'
};

export function reducer(state: State = initialState, action: appActions.Action) {
  switch (action.type) {
    case RooActionTypes.APP_NAME: {
      return {
        ...state
      };
    }
    default:
      return state;
  }
}
