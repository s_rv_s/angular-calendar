# AngularCalendar
Sample Calendar App using angular, ngrx, express, mongo stack
## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

Run ` node ./src/server/server.js ` for a start api server. It's allowed by url `http://localhost:8080/`

##Requires
App required mongodb database named "angularCalendar" on 27027 port at localhost.
On first start API server fill database with initial events

##Credentials. 
username: `user`, password: `pass`
